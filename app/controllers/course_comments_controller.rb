class CourseCommentsController < ApplicationController
  before_action :set_course_comment, only: [:show, :edit, :update, :destroy]

  # GET /course_comments
  # GET /course_comments.json
  def index
    @course_comments = CourseComment.all
  end

  # GET /course_comments/1
  # GET /course_comments/1.json
  def show
    @course_comment = CourseComment.new
  end

  # GET /course_comments/new
  def new
    @course = Course.find params[:course_id]
    @course_comment = CourseComment.new
  end

  # GET /course_comments/1/edit
  def edit
  end

  # POST /course_comments
  # POST /course_comments.json
  def create
    @course = Course.find params[:course_id]
    @course_comment = CourseComment.new(course_comment_params)
    @course_comment.course_id = @course.id
    @course_comment.user_id = current_user.id

    respond_to do |format|
      if @course_comment.save
        format.html { redirect_to @course, notice: 'Course comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @course_comment }
      else
        format.html { redirect_to @course }
        format.json { render json: @course_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /course_comments/1
  # PATCH/PUT /course_comments/1.json
  def update
    respond_to do |format|
      if @course_comment.update(course_comment_params)
        format.html { redirect_to @course_comment, notice: 'Course comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @course_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /course_comments/1
  # DELETE /course_comments/1.json
  def destroy
    @course_comment.destroy
    respond_to do |format|
      format.html { redirect_to course_comments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course_comment
      @course_comment = CourseComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_comment_params
      params.require(:course_comment).permit(:course_comment, :params, :course_id, :user_id, :comment_made)
    end
end
