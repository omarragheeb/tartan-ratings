class SessionsController < ApplicationController

  def new
    if signed_in?
    redirect_to "/landing"
  end
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase , is_verified: true)
    if user && user.authenticate(params[:session][:password])
      sign_in user
      @user= user
    redirect_to "/landing"
    else
      flash.now[:error] = 'Invalid email/password combination'
      render 'new'
    end
  end

 def destroy
    sign_out
    redirect_to root_url
  end

def verify
  uv=UserVerification.find_by(verification_string: params[:vfstring])
  if uv
    user=User.find_by(id: uv.user_id)
    if user
      if user.is_verified
        render 'already_verified'
      else
        user.is_verified = true
        user.save(validate: false)
        render 'verification_success'
      end
    end
  else
    render 'verification_error'
  end
end
end