class CafeteriumCommentsController < ApplicationController
  before_action :set_cafeterium_comment, only: [:show, :edit, :update, :destroy]

  # GET /cafeterium_comments
  # GET /cafeterium_comments.json
  def index
    @cafeterium_comments = CafeteriumComment.all
  end

  # GET /cafeterium_comments/1
  # GET /cafeterium_comments/1.json
  def show
    @cafeterium_comment = CafeteriumComment.new
  end

  # GET /cafeterium_comments/new
  def new
    @cafeterium = Cafeterium.find params[:cafeterium_id]
    @cafeterium_comment = CafeteriumComment.new
  end

  # GET /cafeterium_comments/1/edit
  def edit
  end

  # POST /cafeterium_comments
  # POST /cafeterium_comments.json
  def create
    @cafeterium = Cafeterium.find params[:cafeterium_id]
    @cafeterium_comment = CafeteriumComment.new(cafeterium_comment_params)
    @cafeterium_comment.cafeterium_id = @cafeterium.id
    @cafeterium_comment.user_id = current_user.id

    respond_to do |format|
      if @cafeterium_comment.save
        format.html { redirect_to @cafeterium, notice: 'Cafeterium comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @cafeterium_comment }
      else
        format.html { redirect_to @cafeterium }
        format.json { render json: @cafeterium_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cafeterium_comments/1
  # PATCH/PUT /cafeterium_comments/1.json
  def update
    respond_to do |format|
      if @cafeterium_comment.update(cafeterium_comment_params)
        format.html { redirect_to @cafeterium_comment, notice: 'Cafeterium comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cafeterium_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cafeterium_comments/1
  # DELETE /cafeterium_comments/1.json
  def destroy
    @cafeterium_comment.destroy
    respond_to do |format|
      format.html { redirect_to cafeterium_comments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cafeterium_comment
      @cafeterium_comment = CafeteriumComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cafeterium_comment_params
      params.require(:cafeterium_comment).permit(:cafeterium_comment, :params, :comment_made, :cafeterium_id, :user_id)
    end
end
