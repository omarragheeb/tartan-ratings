class CafeteriaRatingsController < ApplicationController

    def create
            @cafeterium = Cafeterium.find_by_id(params[:cafeterium_id])

                @cafeteria_rating = CafeteriaRating.new(cafeteria_rating_params)
                @cafeteria_rating.cafeterium_id = @cafeterium.id
                @cafeteria_rating.user_id = current_user.id
                if @cafeteria_rating.save
                    respond_to do |format|
                        format.html { redirect_to cafeterium_path(@cafeterium), :notice => "Your rating has been saved" }
                        format.js
                    end
                end
           
        end

        def update
            @cafeterium = Cafeterium.find_by_id(params[:cafeterium_id])
                @cafeteria_rating = current_user.cafeteria_ratings.find_by_cafeterium_id(@cafeterium.id)
                if @cafeteria_rating.update_attributes(cafeteria_rating_params)
                    respond_to do |format|
                        format.html { redirect_to cafeterium_path(@cafeterium), :notice => "Your rating has been updated" }
                        format.js
                    end
                end
            
        end

private
    def cafeteria_rating_params
        params.require(:cafeteria_rating).permit(:value, :cafeterium_id, :user_id)
    end

end
