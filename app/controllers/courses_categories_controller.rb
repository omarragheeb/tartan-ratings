class CoursesCategoriesController < ApplicationController
  before_action :set_courses_category, only: [:show, :edit, :update, :destroy]
  before_action :signed_in_user, only: [:new, :show, :edit, :update, :destroy]

  # GET /courses_categories
  # GET /courses_categories.json
  def index
    @courses_categories = CoursesCategory.all
  end

  # GET /courses_categories/1
  # GET /courses_categories/1.json
  def show
  end

  # GET /courses_categories/new
  def new
    @courses_category = CoursesCategory.new
  end

  # GET /courses_categories/1/edit
  def edit
  end

  # POST /courses_categories
  # POST /courses_categories.json
  def create
    @courses_category = CoursesCategory.new(courses_category_params)

    respond_to do |format|
      if @courses_category.save
        format.html { redirect_to @courses_category, notice: 'Courses category was successfully created.' }
        format.json { render action: 'show', status: :created, location: @courses_category }
      else
        format.html { render action: 'new' }
        format.json { render json: @courses_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses_categories/1
  # PATCH/PUT /courses_categories/1.json
  def update
    respond_to do |format|
      if @courses_category.update(courses_category_params)
        format.html { redirect_to @courses_category, notice: 'Courses category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @courses_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses_categories/1
  # DELETE /courses_categories/1.json
  def destroy
    @courses_category.destroy
    respond_to do |format|
      format.html { redirect_to courses_categories_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_courses_category
      @courses_category = CoursesCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def courses_category_params
      params.require(:courses_category).permit(:name)
    end
        def signed_in_user
      redirect_to signin_url, notice: "Please sign in." unless signed_in?
    end
end
