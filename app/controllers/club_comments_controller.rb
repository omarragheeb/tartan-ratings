class ClubCommentsController < ApplicationController
  before_action :set_club_comment, only: [:show, :edit, :update, :destroy]

  # GET /club_comments
  # GET /club_comments.json
  def index
    @club_comments = ClubComment.all
  end

  # GET /club_comments/1
  # GET /club_comments/1.json
  def show
    @club_comment = ClubComment.new
  end

  # GET /club_comments/new
  def new
    @club = Club.find params[:club_id]
    @club_comment = ClubComment.new
  end

  # GET /club_comments/1/edit
  def edit
  end

  # POST /club_comments
  # POST /club_comments.json
  def create
    @club = Club.find params[:club_id]
    @club_comment = ClubComment.new(club_comment_params)
    @club_comment.club_id = @club.id
    @club_comment.user_id = current_user.id

    respond_to do |format|
      if @club_comment.save
        format.html { redirect_to @club, notice: 'Club comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @club_comment }
      else
        format.html { redirect_to @club }
        format.json { render json: @club_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /club_comments/1
  # PATCH/PUT /club_comments/1.json
  def update
    respond_to do |format|
      if @club_comment.update(club_comment_params)
        format.html { redirect_to @club_comment, notice: 'Club comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @club_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /club_comments/1
  # DELETE /club_comments/1.json
  def destroy
    @club_comment.destroy
    respond_to do |format|
      format.html { redirect_to club_comments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_club_comment
      @club_comment = ClubComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def club_comment_params
      params.require(:club_comment).permit(:club_comment, :params, :comment_made, :club_id, :user_id)
    end
end
