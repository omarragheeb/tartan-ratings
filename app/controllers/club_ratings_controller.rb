class ClubRatingsController < ApplicationController

    def create
            @club = Club.find_by_id(params[:club_id])

                @club_rating = ClubRating.new(club_rating_params)
                @club_rating.club_id = @club.id
                @club_rating.user_id = current_user.id
                if @club_rating.save
                    respond_to do |format|
                        format.html { redirect_to club_path(@club), :notice => "Your rating has been saved" }
                        format.js
                    end
                end
           
        end

        def update
            @club = Club.find_by_id(params[:club_id])
                @club_rating = current_user.club_ratings.find_by_club_id(@club.id)
                if @club_rating.update_attributes(club_rating_params)
                    respond_to do |format|
                        format.html { redirect_to club_path(@club), :notice => "Your rating has been updated" }
                        format.js
                    end
                end
            
        end

private
    def club_rating_params
        params.require(:club_rating).permit(:value, :club_id, :user_id)
    end

end
