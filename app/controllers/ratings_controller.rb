class RatingsController < ApplicationController

    def create
            @course = Course.find_by_id(params[:course_id])

                @rating = Rating.new(rating_params)
                @rating.course_id = @course.id
                @rating.user_id = current_user.id
                if @rating.save
                    respond_to do |format|
                        format.html { redirect_to course_path(@course), :notice => "Your rating has been saved" }
                        format.js
                    end
                end
           
        end

        def update
            @course = Course.find_by_id(params[:course_id])
                @rating = current_user.ratings.find_by_course_id(@course.id)
                if @rating.update_attributes(rating_params)
                    respond_to do |format|
                        format.html { redirect_to course_path(@course), :notice => "Your rating has been updated" }
                        format.js
                    end
                end
            
        end
private
    def rating_params
        params.require(:rating).permit(:value, :course_id, :user_id)
    end

end
