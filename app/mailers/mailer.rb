class Mailer < ActionMailer::Base
  default from: "tartan.ratings15@gmail.com"

  def signup_email(user)
  	vfstring = (0...50).map { ('a'..'z').to_a[rand(26)] }.join

  	uv=UserVerification.new
  	uv.user_id=user.id 
  	uv.verification_string=vfstring 
  	uv.save

  	mail(to: user.email ,

  		body: "verification link: <a href='http://localhost:3000/verify/#{vfstring}' > To verify click here </a>" ,

  		content_type: "text/html",
  		subject: " Tartan Ratings Verification ")

  end
end
