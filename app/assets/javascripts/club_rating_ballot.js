var send = false;
// Sets up the stars to match the data when the page is loaded.
$(function () {
    var checkedId = $('form.club_rating_ballot > input:checked').attr('id');
    $('form.club_rating_ballot > label[for=' + checkedId + ']').prevAll().andSelf().addClass('bright');
});

$(document).ready(function() {
    // Makes stars glow on hover.
    $('form.club_rating_ballot > label').hover(
        function() {    // mouseover
            $(this).prevAll().andSelf().addClass('glow');
        },function() {  // mouseout
            $(this).siblings().andSelf().removeClass('glow');
    });

    // Makes stars stay glowing after click.
    $('form.club_rating_ballot > label').click(function() {
        $(this).siblings().removeClass("bright");
        $(this).prevAll().andSelf().addClass("bright");
    });

    // Submits the form (saves data) after user makes a change.
    $('form.club_rating_ballot input').change(function() {
        if (!send){
        $('form.club_rating_ballot').submit();
        send = true;
        location.reload();
    }
    });
});