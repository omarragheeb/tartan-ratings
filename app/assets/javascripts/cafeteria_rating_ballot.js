var send = false;


// Sets up the stars to match the data when the page is loaded.
$(function () {
    var checkedId = $('form.cafeteria_rating_ballot > input:checked').attr('id');
    $('form.cafeteria_rating_ballot > label[for=' + checkedId + ']').prevAll().andSelf().addClass('bright');
});

$(document).ready(function() {
    // Makes stars glow on hover.
    $('form.cafeteria_rating_ballot > label').hover(
        function() {    // mouseover
            $(this).prevAll().andSelf().addClass('glow');
        },function() {  // mouseout
            $(this).siblings().andSelf().removeClass('glow');
    });

    // Makes stars stay glowing after click.
    $('form.cafeteria_rating_ballot > label').click(function() {
        $(this).siblings().removeClass("bright");
        $(this).prevAll().andSelf().addClass("bright");
    });

    // Submits the form (saves data) after user makes a change.
    $('form.cafeteria_rating_ballot').change(function() {
        if (!send) {
        $('form.cafeteria_rating_ballot').submit();
        send = true;
        location.reload();
    }
    });
});