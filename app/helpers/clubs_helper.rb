module ClubsHelper

def club_rating_ballot
	if @club_rating = current_user.club_ratings.find_by_club_id(params[:id])
		@club_rating
	else
		current_user.club_ratings.new
	end
end

def current_user_club_rating
	if @club_rating = current_user.club_ratings.find_by_club_id(params[:id])
		@club_rating.value
	else
		"N/A"
	end

end

end
