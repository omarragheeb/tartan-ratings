module CafeteriaHelper

def cafeteria_rating_ballot
	if @cafeteria_rating = current_user.cafeteria_ratings.find_by_cafeterium_id(params[:id])
		@cafeteria_rating
	else
		current_user.cafeteria_ratings.new
	end
end

def current_user_cafeteria_rating
	if @cafeteria_rating = current_user.cafeteria_ratings.find_by_cafeterium_id(params[:id])
		@cafeteria_rating.value
	else
		"N/A"
	end

end

end
