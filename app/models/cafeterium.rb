class Cafeterium < ActiveRecord::Base
	has_many :cafeteria_ratings
	has_many :cafeteria_raters, :through => :cafeteria_ratings, :source => :users
	has_many :cafeterium_comments

	belongs_to :cafeteria_category
	
	def cafeteria_average_rating
		@value = 0
		self.cafeteria_ratings.each do |rating|
			@value = @value + rating.value
		end
	
		@total = self.cafeteria_ratings.size
		@value.to_f / @total.to_f
	
	end
	
	def self.get_most_popular
				if self.first.nil?
			return []
		end
						if self.second.nil?
			return []
		end
		max_rating=self.first.cafeteria_average_rating
		c1=self.first
		self.all.each do |cafeterium|
			if cafeterium.cafeteria_average_rating > max_rating
				c1=cafeterium
				max_rating=cafeterium.cafeteria_average_rating
			end
		end
		cc=self.where("id != ?", c1.id)
		max_rating=cc[0].cafeteria_average_rating
		c2=cc[0]
		cc.each do |cafeterium|
			if cafeterium.cafeteria_average_rating > max_rating
				c2=cafeterium
				max_rating=cafeterium.cafeteria_average_rating
			end
		end
		@pop =[c1,c2]
	end 
			def self.by_cafeteria_category(cafeteria_category_id)
		if !cafeteria_category_id.blank?
			self.joins(:cafeteria_category).where('cafeteria.cafeteria_category_id=?', cafeteria_category_id)
		else
			self.all
		end
	end

# filterrific(
#   default_filter_params: { sorted_by: 'created_at_desc' },
#   available_filters: [
#     :cafeteria_category_id,
#   ]
# )

#   scope :cafeteria_category_id, lambda { |cafeteria_category_id|
#   where(cafeteria_category_id: [*cafeteria_category_id])
# }

end
