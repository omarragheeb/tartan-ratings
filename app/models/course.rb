class Course < ActiveRecord::Base

belongs_to :user
# acts_as_commontable

	has_many :ratings
	has_many :raters, :through => :ratings, :source => :users
	has_many :course_comments

	belongs_to :professor
	belongs_to :courses_category

	#, class_name: "CoursesCategory", foreign_key: "category_id"

	def average_rating
		@value = 0
		self.ratings.each do |rating|
			@value = @value + rating.value
		end

		@total = self.ratings.size
		@value.to_f / @total.to_f
	end

	def self.get_most_popular
		if self.first.nil?
			return []
		end
				if self.second.nil?
			return []
		end
		max_rating=self.first.average_rating
		c1=self.first
		self.all.each do |course|
			if course.average_rating > max_rating
				c1=course
				max_rating=course.average_rating
			end
		end
		cc=self.where("id != ?", c1.id)
		max_rating=cc[0].average_rating
		c2=cc[0]
		cc.each do |course|
			if course.average_rating > max_rating
				c2=course
				max_rating=course.average_rating
			end
		end
		@pop =[c1,c2]
	end 
def self.by_courses_category(courses_category_id)
		if !courses_category_id.blank?
			self.joins(:courses_category).where('courses.courses_category_id=?', courses_category_id)
		else
			self.all
		end
	end

end
