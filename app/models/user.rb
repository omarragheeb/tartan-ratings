class User < ActiveRecord::Base
before_save { self.email = email.downcase }
before_create :create_remember_token

has_many :ratings
has_many :rated_courses, :through => :ratings, :source => :courses

has_many :club_ratings
has_many :club_rated_clubs, :through => :club_ratings, :source => :clubs

has_many :cafeteria_ratings
has_many :cafeteria_rated_cafeteria, :through => :cafeteria_ratings, :source => :cafeteria


validates :last_name, presence: true , length: {maximum: 50}
validates :first_name, presence: true , length: {maximum: 50}
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@(qatar\.cmu\.edu)/i
    validates :email, presence: true , format: { with: VALID_EMAIL_REGEX }, 
    uniqueness: {case_sensitive: false}

has_secure_password
validates :password, length: {minimum: 6}
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end
  private

    def create_remember_token
      self.remember_token = User.digest(User.new_remember_token)
    end
end
