json.array!(@courses_categories) do |courses_category|
  json.extract! courses_category, :id, :name
  json.url courses_category_url(courses_category, format: :json)
end
