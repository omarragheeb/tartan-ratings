json.array!(@course_comments) do |course_comment|
  json.extract! course_comment, :id, :comment
  json.url course_comment_url(course_comment, format: :json)
end
