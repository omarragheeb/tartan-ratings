json.array!(@club_comments) do |club_comment|
  json.extract! club_comment, :id, :comment_made, :club_id, :user_id
  json.url club_comment_url(club_comment, format: :json)
end
