json.array!(@cafeterium_comments) do |cafeterium_comment|
  json.extract! cafeterium_comment, :id, :comment_made, :cafeterium_id, :user_id
  json.url cafeterium_comment_url(cafeterium_comment, format: :json)
end
