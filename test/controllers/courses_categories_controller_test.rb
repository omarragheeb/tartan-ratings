require 'test_helper'

class CoursesCategoriesControllerTest < ActionController::TestCase
  setup do
    @courses_category = courses_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:courses_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create courses_category" do
    assert_difference('CoursesCategory.count') do
      post :create, courses_category: { name: @courses_category.name }
    end

    assert_redirected_to courses_category_path(assigns(:courses_category))
  end

  test "should show courses_category" do
    get :show, id: @courses_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @courses_category
    assert_response :success
  end

  test "should update courses_category" do
    patch :update, id: @courses_category, courses_category: { name: @courses_category.name }
    assert_redirected_to courses_category_path(assigns(:courses_category))
  end

  test "should destroy courses_category" do
    assert_difference('CoursesCategory.count', -1) do
      delete :destroy, id: @courses_category
    end

    assert_redirected_to courses_categories_path
  end
end
