# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150421122913) do

  create_table "cafeteria", force: true do |t|
    t.string   "item_name"
    t.string   "item_description"
    t.integer  "item_price"
    t.integer  "cafeteria_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cafeteria_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cafeteria_ratings", force: true do |t|
    t.string   "user_id"
    t.string   "cafeterium_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cafeterium_comments", force: true do |t|
    t.string   "comment_made"
    t.integer  "cafeterium_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cafeterium_comments", ["cafeterium_id"], name: "index_cafeterium_comments_on_cafeterium_id"
  add_index "cafeterium_comments", ["user_id"], name: "index_cafeterium_comments_on_user_id"

  create_table "club_comments", force: true do |t|
    t.string   "comment_made"
    t.integer  "club_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "club_comments", ["club_id"], name: "index_club_comments_on_club_id"
  add_index "club_comments", ["user_id"], name: "index_club_comments_on_user_id"

  create_table "club_ratings", force: true do |t|
    t.string   "user_id"
    t.string   "club_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clubs", force: true do |t|
    t.string   "club_name"
    t.string   "club_description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_comments", force: true do |t|
    t.string   "comment_made"
    t.integer  "course_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "course_comments", ["course_id"], name: "index_course_comments_on_course_id"
  add_index "course_comments", ["user_id"], name: "index_course_comments_on_user_id"

  create_table "courses", force: true do |t|
    t.string   "course_name"
    t.string   "course_number"
    t.string   "course_description"
    t.integer  "professor_id"
    t.integer  "courses_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "year"
  end

  create_table "courses_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "professors", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ratings", force: true do |t|
    t.string   "user_id"
    t.string   "course_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cafeterium_id"
    t.integer  "club_id"
  end

  create_table "user_verifications", force: true do |t|
    t.integer  "user_id"
    t.string   "verification_string"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "major"
    t.string   "year"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "remember_token"
    t.boolean  "is_verified"
    t.boolean  "is_admin"
  end

  add_index "users", ["remember_token"], name: "index_users_on_remember_token"

end
