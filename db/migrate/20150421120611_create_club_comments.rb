class CreateClubComments < ActiveRecord::Migration
  def change
    create_table :club_comments do |t|
      t.string :comment_made
      t.references :club, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
