class CreateCourseComments < ActiveRecord::Migration
  def change
    create_table :course_comments do |t|
      t.string :comment_made
      t.references :course, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
