class CreateCafeteriumComments < ActiveRecord::Migration
  def change
    create_table :cafeterium_comments do |t|
      t.string :comment_made
      t.references :cafeterium, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
