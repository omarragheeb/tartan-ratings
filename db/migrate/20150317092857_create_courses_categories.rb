class CreateCoursesCategories < ActiveRecord::Migration
  def change
    create_table :courses_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
