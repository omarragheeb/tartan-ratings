class CreateUserVerifications < ActiveRecord::Migration
  def change
    create_table :user_verifications do |t|
      t.integer :user_id
      t.string :verification_string

      t.timestamps
    end
  end
end
