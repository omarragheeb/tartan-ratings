class CreateCafeteriaRatings < ActiveRecord::Migration
  def change
    create_table :cafeteria_ratings do |t|
      t.string :user_id
      t.string :cafeterium_id
      t.integer :value

      t.timestamps
    end
  end
end
