class CreateClubRatings < ActiveRecord::Migration
  def change
    create_table :club_ratings do |t|
      t.string :user_id
      t.string :club_id
      t.integer :value

      t.timestamps
    end
  end
end
